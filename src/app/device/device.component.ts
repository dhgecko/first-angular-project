import { DeviceService } from './../services/device.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {

  @Input() id: number
  @Input() name: string
  @Input() status: string
  @Input() index: number

  constructor(private deviceService: DeviceService) { }

  ngOnInit() {
  }

  getColor() {
    if (this.status === 'On') {
      return 'green';
    } else if (this.status === 'Off') {
      return 'red';
    }
  }

  onSwitch() {
    if(this.status === 'On') {
      this.deviceService.switchOffOne(this.index);
    } else if(this.status === 'Off') {
      this.deviceService.switchOnOne(this.index);
    }
  }

  onSave(id: number, name: string, status: string) {
    this.deviceService.updateDevice(id, name, status)
  }

  onDelete(index: number) {
    this.deviceService.deleteDevice(index)
  }
}

