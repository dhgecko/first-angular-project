import {HttpClientModule} from '@angular/common/http';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import {Routes, RouterModule} from '@angular/router';
import { DeviceService } from './services/device.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { DeviceComponent } from './device/device.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthComponent } from './auth/auth.component';
import { DeviceListComponent } from './device-list/device-list.component';
import { SingleDeviceComponent } from './single-device/single-device.component';
import { NotFoundComponent } from './not-found/not-found.component'
import { AuthGuard } from './services/auth-guard.service';
import { EditDeviceComponent } from './edit-device/edit-device.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserItemComponent } from './user-item/user-item.component';
import { NewUserComponent } from './new-user/new-user.component';

const appRoutes: Routes = [
  { path: 'devices', canActivate: [AuthGuard], component: DeviceListComponent },
  { path: 'devices/:id', canActivate:[ AuthGuard], component: SingleDeviceComponent },
  { path: 'edit', canActivate: [AuthGuard], component: EditDeviceComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'users', component: UserListComponent },
  { path: 'new-user', component: NewUserComponent},
  { path: '', component: AuthComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    DeviceComponent,
    AuthComponent,
    DeviceListComponent,
    SingleDeviceComponent,
    NotFoundComponent,
    EditDeviceComponent,
    UserListComponent,
    UserItemComponent,
    NewUserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    DeviceService,
    AuthService,
    AuthGuard,
    UserService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
